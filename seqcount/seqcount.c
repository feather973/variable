#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/kthread.h>
#include <linux/slab.h>

/*
 * seqcount pattern of lockless algorithm to exchange structure
 * (positive interger only)
 *
 * https://lwn.net/Articles/844224/
 */

struct msg
{
	int owner;

	int x;
	int y;
};

struct msg g_msg;		// shared global structure
int sc;					// seqcount

#define N_CONSUMER			(5)
#define N_PRODUCER			(5)
struct task_struct *task[N_CONSUMER+N_PRODUCER];
int task_num[N_CONSUMER+N_PRODUCER];

int consumer(void *arg)
{
	int thread_num = *((int*)arg);
	struct msg copy;
	int old_sc, new_sc;

	while (!kthread_should_stop()) {
		old_sc = smp_load_acquire(&sc);

		copy.x = READ_ONCE(g_msg.x);
		copy.y = READ_ONCE(g_msg.y);
		copy.owner = READ_ONCE(g_msg.owner);
		smp_rmb();

		new_sc = READ_ONCE(sc);
		if (new_sc == old_sc)
			printk("[c%d] x:%d,y:%d,owner:%d\n", thread_num, copy.x, copy.y, copy.owner);
		else
			printk("[c%d] try again (old:%d,new:%d)\n", thread_num, old_sc, new_sc);
			
		msleep(1000);
	}

	return 0;
}

int producer(void *arg)
{
	int thread_num = *((int*)arg);
	struct msg data = { .x = thread_num * 10, .y = thread_num * 100, .owner = thread_num };

	while (!kthread_should_stop()) {
		WRITE_ONCE(sc, sc+1);
		smp_wmb();	

		WRITE_ONCE(g_msg.x, data.x);
		WRITE_ONCE(g_msg.y, data.y);
		WRITE_ONCE(g_msg.owner, data.owner);

		smp_store_release(&sc, sc+1);

		printk("[p%d] x:%d,y:%d\n", thread_num, g_msg.x, g_msg.y);
		msleep(500);
	}

	return 0;
}

static int __init initmodule(void)
{
	int i;

	sc = 0;

	for ( i=0; i<N_CONSUMER; i++ )
	{
		task_num[i] = i;
		task[i] = kthread_run(consumer, (void*)(task_num+i), "consumer-%d", i);
		if (IS_ERR(task[i])) {
			printk("oops, consumer thread %d kthread_run fail\n", i);
			goto exit;
		}
	}

	for ( i=N_CONSUMER; i<N_CONSUMER+N_PRODUCER; i++ )
	{
		task_num[i] = i-N_CONSUMER;
		task[i] = kthread_run(producer, (void*)(task_num+i), "producer-%d", i-N_CONSUMER);
		if (IS_ERR(task[i])) {
			printk("oops, producer thread %d kthread_run fail\n", i-N_CONSUMER);
			goto exit;
		}
	}

exit:
	return 0;
}

static void __exit exitmodule(void)
{
	int i;

	for ( i=0; i<N_CONSUMER+N_PRODUCER; i++ )
	{
		if (task[i])
			kthread_stop(task[i]);
	}

	return;
}

module_init(initmodule);
module_exit(exitmodule);

/* 
 * free for use, but open source code when publish it
 * https://docs.kernel.org/process/license-rules.html
 */
MODULE_LICENSE("GPL v2");		
