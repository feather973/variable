#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/kthread.h>
#include <linux/slab.h>

/*
 * messaing pattern of lockless algorithm to exchange integer variable
 * (positive interger only)
 *
 * https://lwn.net/Articles/844224/
 */

struct msg
{
	int val;
};

struct msg *g_ptr;		// shared global pointer ( no need to perilous parameter copy )

#define N_CONSUMER			(5)
#define N_PRODUCER			(5)
struct task_struct *task[N_CONSUMER+N_PRODUCER];
int task_num[N_CONSUMER+N_PRODUCER];

int consumer(void *arg)
{
	int val;
	struct msg *ptr;
	int thread_num = *((int*)arg);

	while (!kthread_should_stop()) {
		rcu_read_lock();				/* object liveness (ptr) */
		ptr = READ_ONCE(g_ptr);
		smp_rmb();

		val = ptr->val;					/* danger of UAF, use rcu for safe memory */
		rcu_read_unlock();
		if (val < 0) {
			printk("[c%d] errno %d\n", thread_num, val);
		} else {
			printk("[c%d] %d\n", thread_num, val);
		}

		msleep(1000);
	}

	return 0;
}

int producer(void *arg)
{
	int val;
	struct msg *new, *old;
	int thread_num = *((int*)arg);

	while (!kthread_should_stop()) {
		new = kmalloc(sizeof(struct msg), GFP_KERNEL);
		if (!new)
			continue;

		old = READ_ONCE(g_ptr);
		smp_rmb();
		val = old->val;				/* unique among producers */

		printk("[p%d] %d\n", thread_num, thread_num);
		new->val = thread_num;
		smp_wmb();
		WRITE_ONCE(g_ptr, new);

		/* if i create it, i free it ( prevent double free ) */
		if (val == thread_num) {
		//	kfree(old);			/* danger of UAF, RCU need ("call_rcu" or "kfree_rcu" need) */
			kfree_rcu(old);		/* deferred free but danger of oom */
			synchronize_rcu();	/* certificate free, but low performance ( or use "rcu_barrier()" occasionally ) */
		}

		msleep(500);
	}

	return 0;
}

static int __init initmodule(void)
{
	int i;

	g_ptr = kmalloc(sizeof(struct msg), GFP_KERNEL);;
	if (!g_ptr) {
		printk("oops, g_ptr kmalloc fail\n");
		goto exit;
	}

	for ( i=0; i<N_CONSUMER; i++ )
	{
		task_num[i] = i;
		task[i] = kthread_run(consumer, (void*)(task_num+i), "consumer-%d", i);
		if (IS_ERR(task[i])) {
			printk("oops, consumer thread %d kthread_run fail\n", i);
			goto exit;
		}
	}

	for ( i=N_CONSUMER; i<N_CONSUMER+N_PRODUCER; i++ )
	{
		task_num[i] = i-N_CONSUMER;
		task[i] = kthread_run(producer, (void*)(task_num+i), "producer-%d", i-N_CONSUMER);
		if (IS_ERR(task[i])) {
			printk("oops, producer thread %d kthread_run fail\n", i-N_CONSUMER);
			goto exit;
		}
	}

exit:
	return 0;
}

static void __exit exitmodule(void)
{
	int i;

	for ( i=0; i<N_CONSUMER+N_PRODUCER; i++ )
	{
		if (task[i])
			kthread_stop(task[i]);
	}

	if (g_ptr)
		kfree(g_ptr);

	return;
}

module_init(initmodule);
module_exit(exitmodule);

/* 
 * free for use, but open source code when publish it
 * https://docs.kernel.org/process/license-rules.html
 */
MODULE_LICENSE("GPL v2");		
